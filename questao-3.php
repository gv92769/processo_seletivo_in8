<?php
    class Calculo {
  
        private $a;
        private $b;
        private $c;
       
        function getA() {
            return $this->a;
        }

        function setA($a) {
            $this->a = $a;
        }

        function getB() {
            return $this->b;
        }

        function setB($b) {
            $this->b = $b;
        }

        function getC() {
            return $this->c;
        }

        function setC($c) {
            $this->c = $c;
        }

        function multiplicacao() {
            return $this->getA() * $this->getB() * $this->getC();
        }

    }

    $calculo = new Calculo();
    $calculo->setA(2);
    $calculo->setB(4);
    $calculo->setC(5);
    print_r($calculo->multiplicacao());
?>