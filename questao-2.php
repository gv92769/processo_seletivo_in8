<?php
    function divisivel($i) {
        if ($i%2 == 0 && $i%3 == 0 && $i%10 == 0)
            return $i;
        else
            $i = divisivel($i+10);

        return $i;
    }

    print_r(divisivel(10));
?>